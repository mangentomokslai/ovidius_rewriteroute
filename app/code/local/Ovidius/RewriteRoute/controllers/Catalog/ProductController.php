<?php
class Ovidius_RewriteRoute_Catalog_ProductController extends Mage_Core_Controller_Front_Action
{
    public function dumpAction()
    {
        // get product id from url
        $productId  = (int) $this->getRequest()->getParam('id');

        try {
            if ($productId && $productId != 0) {
                // load product
                $product = Mage::getModel('catalog/product')->load($productId);

                if ($product->getId())
                    Zend_Debug::dump($product);
                else
                    throw new Exception('Failed to load product with id ' . $productId);
            } else {
                // throw exception including info about error
                throw new Exception('Product id not provided or invalid');
            }

        } catch (Exception $e) { // catch any thrown error from above try block
            echo $e->getMessage();
            Mage::logException($e);
        }
    }

    /*
        Delete product action
        E.g. /catalog/product/delete/id/123

        This action is hardcoded to be allowed only for customer with id of 1,
        otherwise 'Cannot complete this operation from non-admin area.' exception is thrown
    */
    public function deleteAction()
    {
        $productId  = (int) $this->getRequest()->getParam('id');
        $sessionHelper = Mage::getSingleton('customer/session');
        try {
            if ($productId && $productId != 0) {
                if ($sessionHelper->isLoggedIn()) {
                    $userId = $sessionHelper->getCustomer()->getId();

                    if($userId == '1') // if not the case, exception 'Cannot complete this operation from non-admin area' is thrown
                        Mage::register('isSecureArea', 1);
                }

                $product = Mage::getModel('catalog/product')->load($productId);

                if ($product->getId()) {
                    $product->delete();
                    echo 'Product deleted';
                } else {
                    Mage::throwException('Failed to load product with id ' . $productId);
                }

            } else {
                Mage::throwException('Product id not provided or invalid');
                Mage::logException($e);
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
