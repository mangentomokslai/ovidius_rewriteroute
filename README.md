Module demonstrates how to introduce new controller action (catalog/product/dump/id/[id]) by means of rewriting existing route (router).

In this case we are rewriting Catalog/Product controller. In modules' configuration (config.xml) routers/catalog/modules node we specify our controller and we use [after="Mage_Catalog"] indicating that

it's loaded after original controller.
